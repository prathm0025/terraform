resource "aws_db_instance" "mydb" {
  identifier          = "my-rds-instance"
  allocated_storage   = 20
  storage_type        = "gp2"
  engine              = "mariadb"                                 # Specify MariaDB as the engine
  engine_version      = "10.11.6"                                 # Choose the desired MariaDB version
  instance_class      = "db.t3.micro"
  db_name             = "threetierdb"
  username            = "admin"
  password            = "12345678"
  vpc_security_group_ids = [aws_security_group.my_sg.id]                  # Replace the security group id with your own
  db_subnet_group_name      = aws_db_subnet_group.my_db_subnet_group.name  # Replace the subnet ids with your own
  final_snapshot_identifier = "my-final-snapshot"
  skip_final_snapshot        = false
}

# Create DB subnet group
resource "aws_db_subnet_group" "my_db_subnet_group" {
  name       = "my-db-subnet-group"
  subnet_ids = [aws_subnet.private_subnet_az1.id, aws_subnet.private_subnet_az2.id]                          # Use the subnet where "app-ser" is deployed
} 