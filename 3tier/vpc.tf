provider "aws" {
  region = "eu-west-2"
}

# VPC
resource "aws_vpc" "my_vpc" {
  cidr_block             = "168.167.0.0/20"
  enable_dns_support     = true
  enable_dns_hostnames   = true

  tags = {
    Name = "MyVPC"
  }
}


# Public Subnets Az1
resource "aws_subnet" "public_subnet_az1" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block             = "168.167.1.0/24"
  availability_zone       = "eu-west-2a"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-Subnet-AZ1"
  }
}


# Public subnet Az2
resource "aws_subnet" "public_subnet_az2" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block             = "168.167.2.0/24"
  availability_zone       = "eu-west-2b"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public-Subnet-AZ2"
  }
}


# Private Subnets Az1
resource "aws_subnet" "private_subnet_az1" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block             = "168.167.3.0/24"
  availability_zone       = "eu-west-2a"

  tags = {
    Name = "Private-Subnet-AZ1"
  }
}


# Private Subnet Az2 
resource "aws_subnet" "private_subnet_az2" {
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block             = "168.167.4.0/24"
  availability_zone       = "eu-west-2b"

  tags = {
    Name = "Private-Subnet-AZ2"
  }
}


# public subnet Route Tables
resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }

  tags = {
    Name = "rtb-public"
  }
}

# associat both public subnet in public rtb
resource "aws_route_table_association" "associate_rtb_public_az1" {
  subnet_id      = aws_subnet.public_subnet_az1.id
  route_table_id = aws_route_table.rtb_public.id
}


resource "aws_route_table_association" "associate_rtb_public_az2" {
  subnet_id      = aws_subnet.public_subnet_az2.id
  route_table_id = aws_route_table.rtb_public.id
}  



# Az1 pivate subnet Route Table
resource "aws_route_table" "rtb_private" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway_az1.id
  }

  tags = {
    Name = "rtb-private"
  }
}

resource "aws_route_table_association" "associate_rtb_private_az1" {
  subnet_id      = aws_subnet.private_subnet_az1.id
  route_table_id = aws_route_table.rtb_private.id
}


resource "aws_route_table_association" "associate_rtb_private_az2" {
  subnet_id      = aws_subnet.private_subnet_az2.id
  route_table_id = aws_route_table.rtb_private.id
}


# Internet Gateway
resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "MyIGW"
  }
}

# Az1 NAT Gateways
resource "aws_nat_gateway" "nat_gateway_az1" {
  connectivity_type = "private"
  subnet_id         = aws_subnet.private_subnet_az1.id
}