#launch jump server
resource "aws_instance" "jump-ser" {
  ami           = var.ami
  instance_type = var.itype
  key_name = "pruuuu...."
  vpc_security_group_ids = [aws_security_group.my_sg.id]
  subnet_id = aws_subnet.public_subnet_az1.id
  depends_on = [aws_subnet.public_subnet_az1]

  tags = {
    Name = "Jump-Server"
  }
}


#launch app server
resource "aws_instance" "app-ser" {
  ami           = var.ami
  instance_type = var.itype
  key_name = "pruuuu...."
  vpc_security_group_ids = [aws_security_group.my_sg.id]
  subnet_id = aws_subnet.private_subnet_az1.id
  depends_on = [aws_subnet.private_subnet_az1]

  tags = {
    Name = "App-Server"
  }
}

#security group
resource "aws_security_group" "my_sg" {
    name = "3tier-sg"
    description = "this is my sg"
    vpc_id = aws_vpc.my_vpc.id
    ingress {
        protocol = "TCP"
        from_port = "22"
        to_port = "22"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
       protocol = "TCP"
       from_port = "8080"
       to_port = "8080"
       cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = "80"
        to_port = "80"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol = "TCP"
        from_port = "3306"
        to_port = "3306"
        cidr_blocks = [aws_subnet.private_subnet_az1.cidr_block]
    }
    egress {
        protocol = "-1"
        from_port = "0"
        to_port = "0"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
