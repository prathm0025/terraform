#cidr

variable "my_cidr" {
  default = "192.168.0.0/16"
}

#name_of_project

variable "naam" {
    default = "GAJNI"
}

#define_env

variable "my_env" {
  default = "devlopment"
}

#private_subnet

variable "pri-sub" {
  default = "192.168.0.0/20"
}

#public_subnet
variable "pub-sub" {
  default = "192.168.16.0/20"
}

#ami
variable "my_ami" {
   default = "ami-01d21b7be69801c2f"
}

#instance_type
variable "my_itype" {
  default = "t2.micro"
}

#key_pair
variable "my_key" {
  default = "riteish-deshmukh"
}

