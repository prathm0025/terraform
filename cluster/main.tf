provider "aws" {
region = "eu-west-3"
}


# policy
data "aws_iam_policy_document" "my-cluster-policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

#role
resource "aws_iam_role" "cluster-role" {
  name               = "eks-cluster-role"
  assume_role_policy = data.aws_iam_policy_document.my-cluster-policy.json
}

#attch role
resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.cluster-role.name
}

#policy attach
resource "aws_iam_role_policy_attachment" "AmazonEKSVPCResourceController" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.cluster-role.name
}

#cluster
resource "aws_eks_cluster" "my-cluster" {
  name     = "cluster"
  role_arn = aws_iam_role.cluster-role.arn

  vpc_config {
    subnet_ids = [
        "subnet-0b4dd0fe38d7972a4",
        "subnet-0a49dcf0f5c2cd76a",
        "subnet-02bd24e26fb70e01e"
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.AmazonEKSVPCResourceController,
  ]
}

output "endpoint" {
  value = aws_eks_cluster.my-cluster.endpoint
}
