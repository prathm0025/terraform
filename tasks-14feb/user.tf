provider "aws" {
    region = "eu-west-3"
}

#iam_user
resource "aws_iam_user" "task-user" {
     name = "rowdy-user"
     #path = "/"
     tags = {
        Name = "user"
     }
}

#s3_bucket
resource "aws_s3_bucket" "my-bucket" {
    bucket = "rowdy0025"
    tags = {
        Name = "newbuck"
    }
}

#iam_policy
resource "aws_iam_policy" "test-policy" {
    name = "rowdy-policy"
    policy = jsonencode({
	Version: "2012-10-17",
	Statement : [
		{
			Effect : "Allow",
			Action : "s3:*",
			Resource : aws_s3_bucket.my-bucket.arn,
		},
	],
 })
}

#attach_policy
resource "aws_iam_policy_attachment" "test-attach" {
  name       = "rowdy-attachment"
  users      = [aws_iam_user.task-user.name]
  policy_arn = aws_iam_policy.test-policy.arn
}

#good to go